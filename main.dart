import 'package:flutter/material.dart';
import 'package:login_registration/constants.dart';
import 'package:login_registration/TheScreens/Welcome/welcome_screen.dart';
//import 'package:login_registration/TheScreens/Login/login_screen.dart';

void main(List<String> args) {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'EduXtra',
      theme: ThemeData(
          primaryColor: firstColor, scaffoldBackgroundColor: Colors.pink),
      home: const Welcome(),
    );
  }
}
