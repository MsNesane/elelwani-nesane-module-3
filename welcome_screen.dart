import 'package:flutter/material.dart';
import 'package:login_registration/TheScreens/Welcome/assets/contents.dart';

class Welcome extends StatelessWidget {
  const Welcome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Contents(),
    );
  }
}
