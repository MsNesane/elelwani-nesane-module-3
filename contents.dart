//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_registration/constants.dart';

class Contents extends StatelessWidget {
  const Contents({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_typing_uninitialized_variables
    return RoundButton(
      text: 'LOGIN',
      press: () {},
    );
  }
}

class RoundButton extends StatelessWidget {
  final String text;
  final Function press;
  final Color color, textColor;
  const RoundButton({
    Key? key,
    this.size,
    required this.text,
    required this.press,
    this.color = secondColor,
    this.textColor = Colors.white,
  }) : super(key: key);

  // ignore: prefer_typing_uninitialized_variables
  final size;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        child: ClipRRect(
      borderRadius: BorderRadius.circular(10),
      // ignore: deprecated_member_use
      child: FlatButton(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        color: firstColor,
        onPressed: () {},
        child: Center(
          child: Text(text,
              style: TextStyle(color: textColor), textAlign: TextAlign.right),
        ),
      ),
    ));
  }
}
